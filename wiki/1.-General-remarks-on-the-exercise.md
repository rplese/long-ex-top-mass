# Objective

* Students will be exposed to basic analysis techniques typical of those used at hadron colliders.
* Students will measure the top quark mass utilizing the b-jet energy spectrum i.e. a simple method based on two-body kinematics. 

# Prerequisites

The course design is intended for starting graduate students with little or no prior experience in high energy physics analysis techniques. Some basic particle physics and detector physics will also be assumed, but gladly explained. 

# Miscellaneous Notes

The exercise is divided into a number of steps, which should be completed in the given order. Generally, after each set of steps, the participants will be asked a few short questions. The color scheme of the Exercise is as follows:

Commands or code will be embedded in grey box, e.g., like this for short commands
`pwd`
or like this for longer blocks
```py
# Save the plot
c1.SaveAs("plotData.png")
c1.SaveAs("plotData.pdf")
```

Output and screen printouts will be embedded the same way, e.g.,

`/afs/cern.ch/work/d/dwalter/CMSDAS2024_TopMass/long-ex-top-mass`

Also macros (completed or subject to editing):

`runBJetEnergyPeak.py`

and code to be edited inside a script:

```py
if nJets
```

Quizzes and important messages will be _italic_.
Checkpoints meant as hints or solutions which can help you passing to the next exercise will be in **bold**.


# Course structure outline

The long exercise will be separated into two parts:

* The first part will be an introductory lecture and a group exercise, which will emphasize understanding real CMS data and perform an out-of-the-box version of the first steps of the measurement. Students will follow along with the instructors through a set of short exercises designed to go through the various aspects of the analysis. These exercises will teach students to select events, compare data and Monte Carlo event yields, plot data to Monte Carlo comparisons, and fit the b-jet energy spectrum.
* For the second part the students will choose from three projects mandatory to perform the measurement. In groups of several people, they will complete the corresponding project. Instructors will be available for guidance.
* Results will finally be gathered to create a presentation. Students should thus pay some attention to the cosmetic aspect of their plots during all the exercise. 