import argparse
import os, sys
import ROOT

import pdb

def measureBtagEff(input_filename, output_filename, flavours = ["b", "c", "l"]):
    """
    measure b tagging efficiency for different flavors
    """
    
    hists = {}
    # with uproot.open(input_filename) as inF:

    inF = ROOT.TFile(input_filename, "READ")
    outF = ROOT.TFile(output_filename, "RECREATE")
    for flavor in flavours:
        inF.cd()
        hNum = inF.Get(f"{flavor}tmjetetapt")
        hDen = inF.Get(f"{flavor}mjetetapt")

        new_name = f"eff_{flavor}jetetapt"
        hRatio = hNum.Clone(new_name)
        hRatio.SetDirectory(0)
        hRatio.Divide(hDen)

        outF.cd()
        print(f"Save histogram {new_name} in {output_filename}")
        hRatio.Write()

        # TODO: make some plots

def main():
    """
    steer the script
    """

    # configuration and command line options
    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--inDir', help='input directory', default=None, type=str)
    parser.add_argument('-e', '--era', help='Era to be processed', default="2022", type=str, choices=["2022", "2022EE"])
    parser.add_argument('-o', '--outDir', help='output directory', default='data', type=str)
    args = parser.parse_args()

    measureBtagEff(
        input_filename=f"{args.inDir}/TTto2L2Nu_20{args.era[2:]}.root", 
        output_filename=f"{args.outDir}/eff_btag_TTto2L2Nu_20{args.era[2:]}.root",
        )

if __name__ == "__main__":
    """
    for execution from another script
    """
    sys.exit(main())
