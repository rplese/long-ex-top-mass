#ifndef BJETENERGYPEAK_UTILITIES_H
#define BJETENERGYPEAK_UTILITIES_H

#include <TLorentzVector.h>
#include "TMath.h"
#include <ROOT/RVec.hxx>
#include <ROOT/RDataFrame.hxx>
#include <ROOT/RDF/RInterface.hxx>
#include <TH2.h>
#include <TFile.h>

#include "correction.h"
#include <unordered_map>
#include <memory>
#include <algorithm>
#include <stdexcept>

using Vec_c = ROOT::VecOps::RVec<unsigned char>;
using Vec_short = ROOT::VecOps::RVec<short>;
using Vec_i = ROOT::VecOps::RVec<int>;
using Vec_f = ROOT::VecOps::RVec<float>;

using LorentzVector = ROOT::Math::PtEtaPhiMVector;
using Vec_lorentz = ROOT::VecOps::RVec<LorentzVector>;

namespace helpers {

Vec_f energy(const Vec_f& pt, const Vec_f& eta, const Vec_f& phi, const Vec_f& mass) {
    Vec_f res(pt.size());
    for (unsigned int i = 0; i < res.size(); ++i) {
        res[i] = LorentzVector(pt[i], eta[i], phi[i], mass[i]).E();
    }
    return res;
}

Vec_f lne(const Vec_f& energy) {
    Vec_f res(energy.size());
    for (unsigned int i = 0; i < res.size(); ++i) {
        res[i] = TMath::Log(energy[i]);
    }
    return res;
}

float top_pt_reweighting(const float top_pt, const float antitop_pt) {
  // numbers taken from TOP-23-008 (http://cms.cern.ch/iCMS/jsp/openfile.jsp?tp=draft&files=AN2024_019_v9.pdf)
  return std::sqrt(
    (0.103*std::exp(-0.0118*top_pt)-0.000134*top_pt + 0.973) * (0.991 + 0.000075*top_pt) *
    (0.103*std::exp(-0.0118*antitop_pt)-0.000134*antitop_pt + 0.973) * (0.991 + 0.000075*antitop_pt)
  ); 
}

}

class JsonHelper {
public:
  using pair_t = std::pair<unsigned int, unsigned int>;
  using jsonmap_t = std::unordered_map<unsigned int, std::vector<pair_t>>;
  
  JsonHelper(const std::vector<unsigned int> &runs, const std::vector<unsigned int> &firstlumis, const std::vector<unsigned int> &lastlumis
  ) : jsonmap_(std::make_shared<jsonmap_t>()) 
  {
    for (unsigned int i = 0; i < firstlumis.size(); ++i) {
      (*jsonmap_)[runs[i]].push_back(std::make_pair(firstlumis[i],lastlumis[i]));
    }
    
    for (auto &item : *jsonmap_) {
      std::sort(item.second.begin(), item.second.end());
    }
  }
  
  bool operator () (unsigned int run, unsigned int lumi) const {
    if (run == 1) {
      return true;
    }
    
    const auto it = jsonmap_->find(run);
    if (it != jsonmap_->end()) {
      auto const &pairs = it->second;
      auto const pairit = std::lower_bound(pairs.begin(), pairs.end(), lumi, [](const pair_t &pair, unsigned int val) { return pair.second < val; } );
      if (pairit != pairs.end()) {
        if (lumi >= pairit->first) {
          return true;
        }
      }
    }
    return false;
  }
  

private:
  std::shared_ptr<jsonmap_t> jsonmap_;
};


class JetVetoHelper {
public:  
  JetVetoHelper(const std::string file_name, const std::string correction_name) {
    vetoMaps_ = correction::CorrectionSet::from_file(file_name)->at(correction_name);
  }
  
  bool operator () (const Vec_f& eta, const Vec_f& phi) const {
    for (unsigned int i = 0; i < eta.size(); ++i) {
        if(vetoMaps_->evaluate({"jetvetomap", std::clamp(eta[i], -4.7f, 4.7f), std::clamp(phi[i], -3.1415f, 3.1415f)}) != 0)
          return false;
    }
    return true;
  }
private:
  correction::Correction::Ref vetoMaps_;
};


class JERCHelper {
public:  
  JERCHelper(const std::string file_name, const std::string correction_name) {
    auto csetJEC = correction::CorrectionSet::from_file(file_name);
    jecMaps_ = csetJEC->compound().at(correction_name);
  }
  
  Vec_f operator () (const Vec_f& area, const Vec_f& eta, const Vec_f& pt, const float rho) const {
    Vec_f res;
    res.reserve(eta.size());
    for (unsigned int i = 0; i < eta.size(); ++i) {
        res.emplace_back(jecMaps_->evaluate({area[i], eta[i], pt[i], rho}));
    }
    return res;
  }


private:
  correction::CompoundCorrection::Ref jecMaps_;
};

class JESVarHelper {
public:  
  JESVarHelper(const std::string file_name, const std::string correction_name) {
    auto csetJEC = correction::CorrectionSet::from_file(file_name);
    jecMaps_ = csetJEC->at(correction_name);
  }
  
  Vec_f operator () (const Vec_f& eta, const Vec_f& pt) const {
    Vec_f res;
    res.reserve(eta.size());
    for (unsigned int i = 0; i < eta.size(); ++i) {
        res.emplace_back(jecMaps_->evaluate({eta[i], pt[i]}));
    }
    return res;
  }

private:
  correction::Correction::Ref jecMaps_;
};

class JERVarHelper {
public:  
  JERVarHelper(
    const std::string file_name, 
    const std::string correction_name, 
    const std::string sf_name, 
    const std::string file_name_smeartool, 
    const std::string smeartool_name,
    const std::string variation
  ) : variation(variation) 
  {
    auto csetJEC = correction::CorrectionSet::from_file(file_name);
    jerMaps_ = csetJEC->at(correction_name);
    sfMaps_ = csetJEC->at(sf_name);
    smearTool_ = correction::CorrectionSet::from_file(file_name_smeartool)->at(smeartool_name);
  }
  
  Vec_f operator () (const Vec_f& eta, const Vec_f& pt, const float rho, const Vec_short& idxs, const Vec_f& ptGen) const {
    Vec_f res;
    res.reserve(eta.size());
    for (unsigned int i = 0; i < eta.size(); ++i) {
        const double jer = jerMaps_->evaluate({eta[i], pt[i], rho});
        const double jer_sf = sfMaps_->evaluate({eta[i], pt[i], variation});
        const double corr = smearTool_->evaluate({pt[i], eta[i], ptGen[idxs[i]], rho, 12345, jer, jer_sf});

        res.emplace_back(corr);
    }
    return res;
  }

private:
  correction::Correction::Ref jerMaps_;
  correction::Correction::Ref sfMaps_;
  correction::Correction::Ref smearTool_;
  const std::string variation;
};


class BTagHelper {
public:  
  BTagHelper(
    const std::string sf_file_name, 
    const std::string correction_name, 
    const std::string eff_file_name, 
    const std::string eff_name, 
    const std::string variation, 
    const std::string wp,
    const int flavor
  ): variation(variation), wp(wp), flavor(flavor) {

    sfMaps_ = correction::CorrectionSet::from_file(sf_file_name)->at(correction_name);

    std::unique_ptr<TFile> tFile(TFile::Open(eff_file_name.c_str(), "READ"));

    hEff_ = (TH2D*)tFile->Get(eff_name.c_str());
    hEff_->SetDirectory(0);
  }
  
  double operator () (const Vec_f& abseta, const Vec_f& pt, const Vec_i& tagged) const {
    double p_mc = 1.0;
    double p_data = 1.0;

    double sf, eff;
    for (unsigned int i = 0; i < abseta.size(); ++i) {
      sf = sfMaps_->evaluate({variation, wp, flavor, abseta[i], pt[i]});
      eff = hEff_->GetBinContent(hEff_->GetXaxis()->FindBin(abseta[i]), hEff_->GetYaxis()->FindBin(pt[i]));

      if(tagged[i]){
        p_mc *= eff;
        p_data *= sf*eff;
      }
      else{
        p_mc *= (1-eff);
        p_data *= (1-sf*eff);
      }
    }
    return p_data/p_mc;
  }
private:
  correction::Correction::Ref sfMaps_;
  TH2D* hEff_;
  const std::string variation;
  const std::string wp;
  const int flavor;
};



#endif