import argparse
import os, sys
import ROOT
import pathlib
import json
from array import array
import itertools

import resources as res
from utilities import buildFileListXrd

import correctionlib

import pdb

# disable root warnings
ROOT.gErrorIgnoreLevel=ROOT.kError

def runBJetEnergyPeak(filelist, era, sample, outFileURL, measure_btag_eff=False, helpers={}, do_systematic_variations=True):
    """
    Perform the analysis on a single sample
    """
    
    sample_parts = sample.split("_")
    sample_base = sample_parts[0]
    if len(sample_parts) > 1:
        sample_mode = "_".join(sample_parts[1:])
    else:
        sample_mode = None

    is_data = sample_base in ["MuonEG", "Muon", "EGamma"]
    print(f"Process {'Data' if is_data else 'MC'} sample {sample}")

    # sum of weights before selection, needed to normalize MC
    if not is_data:
        rdf_run = ROOT.RDataFrame('Runs', filelist)
        weightsum = rdf_run.Sum("genEventSumw").GetValue()
        print(f"Sum of weights: {weightsum}")

    histos = [] # list of histograms for control plots
    histsVaried = [] # list of histograms for measurement containing all systematic variations

    rdf_base = ROOT.RDataFrame('Events', filelist)
    
    nEvents = rdf_base.Count().GetValue()
    print(f"Number of events: {nEvents}")
    
    if is_data:
        rdf_base = rdf_base.Filter(helpers["json"], ["run", "luminosityBlock"], "jsonhelper")

        # trigger selection, ensure to take each data event only once
        if sample_base == "MuonEG":
            rdf_base = rdf_base.Filter("HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ || HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL")
        elif sample_base == "Muon":
            rdf_base = rdf_base.Filter("!(HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ || HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL) && HLT_IsoMu24")
        elif sample_base == "EGamma":
            rdf_base = rdf_base.Filter("!(HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ || HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL || HLT_IsoMu24) && HLT_Ele32_WPTight_Gsf")
    else:
        rdf_base.Filter("HLT_Mu8_TrkIsoVVL_Ele23_CaloIdL_TrackIdL_IsoVL_DZ || HLT_Mu23_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL || HLT_IsoMu24 || HLT_Ele32_WPTight_Gsf") 

    # event filters https://twiki.cern.ch/twiki/bin/viewauth/CMS/MissingETOptionalFiltersRun2#Run_3_recommendations 
    rdf_base = rdf_base.Filter("Flag_goodVertices && Flag_globalSuperTightHalo2016Filter && Flag_EcalDeadCellTriggerPrimitiveFilter && Flag_BadPFMuonFilter && Flag_BadPFMuonDzFilter && Flag_hfNoisyHitsFilter && Flag_eeBadScFilter")

    # event selection, leptons
    if era == "2022EE":
        # veto the EE leak region for data and MC
        ele_EEveto = " && (((Electron_eta + Electron_deltaEtaSC) < 1.56) || (Electron_seediEtaOriX > 45) || (Electron_seediPhiOriY < 72))"
    else:
        ele_EEveto = ""

    rdf_base = rdf_base.Define("GoodElectrons", "Electron_cutBased>=4 && Electron_miniPFRelIso_all<0.15 && Electron_pt > 20 && Electron_pt < 200 && abs(Electron_eta)<2.5" + ele_EEveto) \
        .Filter("Sum(GoodElectrons) == 1") \
        .Define("GoodMuons", "Muon_tightId && Muon_miniPFRelIso_all<0.15 && Muon_pt > 20 && Muon_pt < 200 && abs(Muon_eta)<2.4") \
        .Filter("Sum(GoodMuons) == 1") \
        .Filter("Muon_charge[GoodMuons][0] + Electron_charge[GoodElectrons][0] == 0") \
        .Filter("(ROOT::Math::PtEtaPhiMVector(Muon_pt[GoodMuons][0], Muon_eta[GoodMuons][0], Muon_phi[GoodMuons][0], 0.105658) + \
            ROOT::Math::PtEtaPhiMVector(Electron_pt[GoodElectrons][0], Electron_eta[GoodElectrons][0], Electron_phi[GoodElectrons][0], 0.000511)).mass() > 12")

    # baseline jet selection (independent of jet energy corrections)
    wp_btag = res.wp_btag_transformer_medium[era]
    rdf_base = rdf_base.Define("VetoJets", "Jet_jetId==6 && abs(Jet_eta)<4.7") \
        .Define("VetoJet_eta", "Jet_eta[VetoJets]") \
        .Define("VetoJet_phi", "Jet_phi[VetoJets]") \
        .Filter(helpers["jetveto"], ["VetoJet_eta", "VetoJet_phi"], "jetvetohelper")
    
    rdf_base = rdf_base.Define("TightJets", "Jet_jetId==6 && abs(Jet_eta)<2.5") \
        .Filter("Sum(TightJets) >= 2") \
        .Define("TightBJets", f"TightJets && Jet_btagRobustParTAK4B>{wp_btag}") \
        .Filter("Sum(TightBJets) >= 1")

    # event weights (independent of jet selection)
    if not is_data:
        # list of tuples with the nominal weight and the weight for repacement
        weight_variations = []
            
        # pileup weight
        rdf_base = rdf_base.Define("weight_pileup", 'corr_pileup->evaluate({{Pileup_nTrueInt, "nominal"}})')
        if do_systematic_variations:
            for var in ["up", "down"]:
                rdf_base = rdf_base.Define("weight_pileup_"+var, 'corr_pileup->evaluate({{Pileup_nTrueInt, "'+var+'"}})')
                weight_variations.append(("weight_pileup", "weight_pileup_"+var))
            
        # muon ID and isolation
        rdf_base = rdf_base.Define("weight_muonID", 'corr_muonID->evaluate({{Muon_eta[GoodMuons][0], Muon_pt[GoodMuons][0], "nominal"}})')
        if do_systematic_variations:
            for var, varname in [("stat", "stat_up"), ("systdown", "syst_down"), ("systup", "syst_up")]:
                rdf_base = rdf_base.Define("weight_muonID_"+varname, 'corr_muonID->evaluate({{Muon_eta[GoodMuons][0], Muon_pt[GoodMuons][0], "'+var+'"}})')
                weight_variations.append(("weight_muonID", "weight_muonID_"+varname))

        # electron reco and ID/isolation
        era_tag = '2022Re-recoBCD' if era=="2022" else '2022Re-recoE+PromptFG'
        rdf_base = rdf_base.Define("weight_eleReco", 'Electron_pt[GoodElectrons][0] < 75 \
                ? corr_eleID->evaluate({{"'+era_tag+'", "sf", "Reco20to75", Electron_eta[GoodElectrons][0], Electron_pt[GoodElectrons][0]}}) \
                : corr_eleID->evaluate({{"'+era_tag+'", "sf", "RecoAbove75", Electron_eta[GoodElectrons][0], Electron_pt[GoodElectrons][0]}})')
        rdf_base = rdf_base.Define("weight_eleID", 'corr_eleID->evaluate({{"'+era_tag+'", "sf", "Tight", Electron_eta[GoodElectrons][0], Electron_pt[GoodElectrons][0]}})')

        if do_systematic_variations:
            for var, varname in [("sfdown", "up"), ("sfup", "down")]:
                rdf_base = rdf_base.Define("weight_eleID_"+varname, 'corr_eleID->evaluate({{"'+era_tag+'", "'+var+'", "Tight", Electron_eta[GoodElectrons][0], Electron_pt[GoodElectrons][0]}})')
                weight_variations.append(("weight_eleID", "weight_eleID_"+varname))

                rdf_base = rdf_base.Define("weight_eleReco_"+varname, 'Electron_pt[GoodElectrons][0] < 75 \
                    ? corr_eleID->evaluate({{"'+era_tag+'", "'+var+'", "Reco20to75", Electron_eta[GoodElectrons][0], Electron_pt[GoodElectrons][0]}}) \
                    : corr_eleID->evaluate({{"'+era_tag+'", "'+var+'", "RecoAbove75", Electron_eta[GoodElectrons][0], Electron_pt[GoodElectrons][0]}})')

                weight_variations.append(("weight_eleReco", "weight_eleReco_"+varname))

        base_weights = ['genWeight', 'weight_pileup' , 'weight_muonID', 'weight_eleReco', 'weight_eleID'] 

        # top pT reweighting applied to all ttbar samples
        if sample.startswith("TT"):
            rdf_base = rdf_base.Define("weight_topPt", "helpers::top_pt_reweighting(\
                    GenPart_pt[GenPart_pdgId==6 && GenPart_statusFlags&(1<<8) && GenPart_statusFlags&(1<<13)][0], \
                    GenPart_pt[GenPart_pdgId==-6 && GenPart_statusFlags&(1<<8) && GenPart_statusFlags&(1<<13)][0])")
            base_weights.append("weight_topPt")

    # Apply jet energy corrections
    rdf_base = rdf_base.Define("TightJet_rawPt", "Jet_pt[TightJets]*(1-Jet_rawFactor[TightJets])") \
        .Define("TightJet_area","Jet_area[TightJets]") \
        .Define("TightJet_eta","Jet_eta[TightJets]")

    if is_data:
        if era == "2022":
            helper_jerc = helpers["jerc_data"]
        elif sample_mode=="E":
            helper_jerc = helpers["jerc_dataE"]
        elif sample_mode=="F":
            helper_jerc = helpers["jerc_dataF"]
        elif sample_mode=="G":
            helper_jerc = helpers["jerc_dataG"]
        else:
            raise RuntimeError(f"Unknown JERC for sample {sample}!")
    else:
        helper_jerc = helpers["jerc_mc"]

    rdf_base = rdf_base.Define("TightJet_correction_nominal", helper_jerc, ["TightJet_area", "TightJet_eta", "TightJet_rawPt", "Rho_fixedGridRhoFastjetAll"], "jerc_helper")

    jerc_variations = []
    if not is_data and do_systematic_variations:
        jerc_variations = res.jec_variations[:]
        jerc_variations.append("ptResolution")


    # loop over nominal jet energy correction and the variations
    for jec_var_sign, jec_var in [(None, None), *[(x,y) for x,y in itertools.product(("up","down"), jerc_variations)]]:
        if is_data:
            print("Adding graph with nominal jet energy corrections")
            rdf = rdf_base.Alias("TightJet_correction", "TightJet_correction_nominal")
        else:
            if jec_var is None:
                print("Adding graph with nominal jet energy & resolution corrections")
                do_weight_variations = do_systematic_variations

                # smear pT for jet energy resolution corection
                rdf = rdf_base.Define("TightJet_ptCorr", "TightJet_correction_nominal * Jet_pt[TightJets]") \
                    .Define("TightJet_genIdx", "Jet_genJetIdx[TightJets]") \
                    .Define(f"TightJet_smear", 
                        helpers[f"jer_mc"], 
                        ["TightJet_eta", "TightJet_ptCorr", "Rho_fixedGridRhoFastjetAll", "TightJet_genIdx", "GenJet_pt"], 
                        "jerc_helper"
                    )
                rdf = rdf.Define("TightJet_correction", "TightJet_correction_nominal * TightJet_smear")
            else:
                print(f"Adding graph with jet energy & resolution corrections {jec_var} {jec_var_sign}")
                do_weight_variations = False

                if jec_var in ["ptResolution"]:
                    rdf = rdf_base.Define("TightJet_ptCorr", "TightJet_correction_nominal * Jet_pt[TightJets]") \
                        .Define("TightJet_genIdx", "Jet_genJetIdx[TightJets]") \
                        .Define(f"TightJet_smear", 
                            helpers[f"jer_mc_{jec_var}_{jec_var_sign}"], 
                            ["TightJet_eta", "TightJet_ptCorr", "Rho_fixedGridRhoFastjetAll", "TightJet_genIdx", "GenJet_pt"], 
                            "jerc_helper"
                        )
                    rdf = rdf.Define("TightJet_correction", "TightJet_correction_nominal * TightJet_smear")
                else:
                    if f"TightJet_correction_{jec_var}" not in rdf_base.GetColumnNames():
                        rdf_base = rdf_base.Define(f"TightJet_correction_{jec_var}", helpers[f"jerc_mc_{jec_var}"], ["TightJet_eta", "TightJet_rawPt"], "jerc_helper")

                    if jec_var_sign == "up":
                        rdf = rdf_base.Define("TightJet_correction_var", f"TightJet_correction_nominal * (1+TightJet_correction_{jec_var})")
                    else:
                        rdf = rdf_base.Define("TightJet_correction_var", f"TightJet_correction_nominal * (1-TightJet_correction_{jec_var})")

                    rdf = rdf.Define("TightJet_ptCorr", "TightJet_correction_var * Jet_pt[TightJets]") \
                        .Define("TightJet_genIdx", "Jet_genJetIdx[TightJets]") \
                        .Define(f"TightJet_smear", 
                            helpers[f"jer_mc"], 
                            ["TightJet_eta", "TightJet_ptCorr", "Rho_fixedGridRhoFastjetAll", "TightJet_genIdx", "GenJet_pt"], 
                            "jerc_helper"
                        )

                    rdf = rdf.Define("TightJet_correction", "TightJet_correction_var * TightJet_smear")

        # event selection, jets
        rdf = rdf.Define("TightJet_pt", "TightJet_correction * Jet_pt[TightJets]") \
            .Define("TightJet_phi", "Jet_phi[TightJets]") \
            .Define("TightJet_mass", "TightJet_correction * Jet_mass[TightJets]") \
            .Define("TightJet_good", "TightJet_pt > 30") \
            .Define("TightJet_tagged", "TightJet_good && TightBJets[TightJets]")

        rdf = rdf.Filter("Sum(TightJet_good) >= 2") 
        if not measure_btag_eff:
            rdf = rdf.Filter("Sum(TightJet_tagged) == 1 || Sum(TightJet_tagged) == 2")

        rdf = rdf.Define("GoodJet_pt", "TightJet_pt[TightJet_good]") \
            .Define("GoodJet_eta", "Jet_eta[TightJets][TightJet_good]") \
            .Define("GoodJet_phi", "Jet_phi[TightJets][TightJet_good]") \
            .Define("GoodJet_mass", "Jet_mass[TightJets][TightJet_good]") \
            .Define("GoodJet_tagged", "TightJet_tagged[TightJet_good]")

        # b jet energy
        rdf = rdf.Define("GoodTaggedJets_energy", "helpers::energy(GoodJet_pt[GoodJet_tagged], GoodJet_eta[GoodJet_tagged], GoodJet_phi[GoodJet_tagged], GoodJet_mass[GoodJet_tagged])")
        rdf = rdf.Define("GoodTaggedJets_lne", "helpers::lne(GoodTaggedJets_energy)")

        # define event weights
        if is_data:
            rdf = rdf.Define("weight", "1.0")
        else:
            event_weights = base_weights[:]
            event_weight_variations = weight_variations[:]

            rdf = rdf.Define("BJets", "Jet_hadronFlavour[TightJets][TightJet_good] == 5") \
                .Define("CJets", "Jet_hadronFlavour[TightJets][TightJet_good] == 4") \
                .Define("LightJets", "Jet_hadronFlavour[TightJets][TightJet_good] == 0")

            if not measure_btag_eff and "btag_b" in helpers.keys():
                # evaluate b-tagging scale factors
                rdf = rdf.Define("GoodBJet_abseta", "abs(GoodJet_eta[BJets])") \
                    .Define("GoodBJet_pt", "GoodJet_pt[BJets]") \
                    .Define("GoodBJet_tagged", "GoodJet_tagged[BJets]") \
                    .Define("weight_btag_b", helpers["btag_b"], ["GoodBJet_abseta", "GoodBJet_pt", "GoodBJet_tagged"], "btaghelper_b") \
                    .Define("GoodCJets_abseta", "abs(GoodJet_eta[CJets])") \
                    .Define("GoodCJets_pt", "GoodJet_pt[CJets]") \
                    .Define("GoodCJets_tagged", "GoodJet_tagged[CJets]") \
                    .Define("weight_btag_c", helpers["btag_c"], ["GoodCJets_abseta", "GoodCJets_pt", "GoodCJets_tagged"], "btaghelper_c") \
                    .Define("weight_btag", "weight_btag_b * weight_btag_c") \
                    .Define("GoodLJets_abseta", "abs(GoodJet_eta[LightJets])") \
                    .Define("GoodLJets_pt", "GoodJet_pt[LightJets]") \
                    .Define("GoodLJets_tagged", "GoodJet_tagged[LightJets]") \
                    .Define("weight_btag_light", helpers["btag_light"], ["GoodLJets_abseta", "GoodLJets_pt", "GoodLJets_tagged"], "btaghelper_light")
                event_weights.append("weight_btag")
                event_weights.append("weight_btag_light")

                if do_weight_variations:

                    for var in ["down","up"]:
                        rdf = rdf.Define(f"weight_btag_light_{var}", helpers[f"btag_light_{var}"], ["GoodLJets_abseta", "GoodLJets_pt", "GoodLJets_tagged"], f"btaghelper_light_{var}")
                        event_weight_variations.append(("weight_btag_light", f"weight_btag_light_{var}"))
                    
                    for var, varname in [
                        ("up_uncorrelated", f"{era}_up"), 
                        ("down_uncorrelated", f"{era}_down"),
                        ("up_correlated", "up"), 
                        ("down_correlated", "down")
                    ]:
                        rdf = rdf.Define(f"weight_btag_b_{var}", helpers[f"btag_b_{var}"], ["GoodBJet_abseta", "GoodBJet_pt", "GoodBJet_tagged"], f"btaghelper_b_{var}") \
                            .Define(f"weight_btag_c_{var}", helpers[f"btag_c_{var}"], ["GoodCJets_abseta", "GoodCJets_pt", "GoodCJets_tagged"], f"btaghelper_c_{var}") \
                            .Define(f"weight_btag_{varname}", f"weight_btag_b_{var} * weight_btag_c_{var}")
                        event_weight_variations.append(("weight_btag", f"weight_btag_{varname}"))

            if jec_var is None:
                print(f'Central weight will be composed of: {event_weights}')
            event_weight_str = " * ".join(event_weights)
            rdf = rdf.Define("weight", event_weight_str)

            if do_weight_variations:
                # weight based variations
                varWgtNames = []
                varWgtList = []
                # theory uncertainties for ttbar samples
                if sample.startswith("TT"):
                    # Parton shower variations
                    for i,name in enumerate(["ISR_up","FSR_up","ISR_down","FSR_down"]):
                        varWgtNames.append(name)
                        varWgtList.append(" * ".join([*event_weights, f"PSWeight[{i}]"]))
                    # Matrix element scale variations
                    for i,name in [(1,"muR_down"), (3,"muF_down"), (4,"muF_up"), (6,"mur_up"),]:
                        varWgtNames.append(name)
                        varWgtList.append(" * ".join([*event_weights, f"LHEScaleWeight[{i}]"]))
                    # PDF variations (one sided)
                    for i in range(1,101):
                        varWgtNames.append(f"pdf{i}_up")
                        varWgtList.append(" * ".join([*event_weights, f"LHEPdfWeight[{i}]"]))
                    # PDF alpha(S) variations
                    for i,name in [(101, "alphaS_up"), (102, "alphaS_down")]:
                        varWgtNames.append(name)
                        varWgtList.append(" * ".join([*event_weights, f"LHEPdfWeight[{i}]"]))

                    # turn off pT top reweighting
                    varWgt = event_weights[:]
                    varWgt.remove("weight_topPt")
                    varWgtNames.append("topPt_down")
                    varWgtList.append(" * ".join(varWgt))

                # experimental uncertainties, for each variation an existing weight needs to be replaced
                for nominal, variation in event_weight_variations:  
                    # index of central eight to be replaced
                    idx = event_weights.index(nominal)
                    varWgt=event_weights.copy()
                    #substitute one of the weights by the alternative
                    varWgt[idx] = variation
                    #add to list
                    varWgtNames.append(variation.replace("weight_",""))
                    varWgtList.append('*'.join(varWgt))

                varWgtStr=','.join(varWgtList)
                rdf = rdf.Vary("weight", f"ROOT::RVecD{{{varWgtStr}}}", varWgtNames)

        # create and fill histograms
        if jec_var is None:
            histos.extend([
                rdf.Histo1D(('nvtx',';Vertex multiplicity; Events',100,0,100), "PV_npvsGood", "weight"),
                rdf.Define("nGoodJets", "Sum(TightJet_good)") \
                    .Histo1D(('njets',';jet multiplicity; Events',7,1,8), 'nGoodJets', "weight"),
                rdf.Define("nGoodTaggedJets", "Sum(GoodJet_tagged)") \
                    .Histo1D(('nbtags',';b-tag multiplicity; Events',4,0,4), 'nGoodTaggedJets', "weight"),
                rdf.Histo1D(('jetpt',';Transverse Momentum; Jets',50,30,300), 'GoodJet_pt', "weight"),
                rdf.Histo1D(('jeteta',';Pseudorapidity; Jets',50,-2.5,2.5), 'GoodJet_eta', "weight"),
                rdf.Define("GoodTaggedJet_pt", "GoodJet_pt[GoodJet_tagged]") \
                    .Histo1D(('bjetpt',';Transverse Momentum; b-tagged Jets',50,30,300), 'GoodTaggedJet_pt', "weight"),
                rdf.Define("GoodTaggedJet_eta", "GoodJet_eta[GoodJet_tagged]") \
                    .Histo1D(('bjeteta',';Pseudorapidity; b-tagged Jets',50,-2.5,2.5), 'GoodTaggedJet_eta', "weight"),
                rdf.Histo1D(('bjeten',';Energy [GeV]; Jets',30,0,300), 'GoodTaggedJets_energy', "weight"),
                rdf.Define("GoodElectron_pt", "Electron_pt[GoodElectrons][0]") \
                    .Histo1D(('elept',';Electron Transverse Momentum; Events',36,20,200), "GoodElectron_pt", "weight"),
                rdf.Define("GoodElectron_eta", "Electron_eta[GoodElectrons][0]") \
                    .Histo1D(('eleeta',';Electron Pseudorapidity; Events',50,-2.5,2.5), "GoodElectron_eta", "weight"),
                rdf.Define("GoodMun_pt", "Muon_pt[GoodMuons][0]") \
                    .Histo1D(('mupt',';Muon Transverse Momentum; Events',36,20,200), "GoodMun_pt", "weight"),
                rdf.Define("GoodMuon_eta", "Muon_eta[GoodMuons][0]") \
                    .Histo1D(('mueta',';Muon Pseudorapidity; Events',50,-2.5,2.5), "GoodMuon_eta", "weight"),
                rdf.Histo1D(('metpt',';MET [GeV]; Events',55,0.,1100.), "PuppiMET_pt", "weight"),
            ])

            if measure_btag_eff:          
                # histograms needed to measure b-tagging efficiency
                bins_abseta = [5, array('d', [0, 0.4, 0.8, 1.2, 1.8, 2.5])]
                bins_pt = [7, array('d', [30,40,50,70,100,140,200,300])]
                bins = [*bins_abseta, *bins_pt]

                histos.extend([
                    rdf.Define("MatchedBJets_abseta", "abs(GoodJet_eta[BJets])") \
                        .Define("MatchedBJets_pt", "abs(GoodJet_pt[BJets])") \
                        .Histo2D(
                            ROOT.RDF.TH2DModel('bmjetetapt',';abs(eta) vs. pT (b jet); Jets', *bins), 
                            'MatchedBJets_abseta', "MatchedBJets_pt", "weight"
                            ),
                    rdf.Define("GoodMatchedBJets_abseta", "abs(GoodJet_eta[BJets && GoodJet_tagged])") \
                        .Define("GoodMatchedBJets_pt", "abs(GoodJet_pt[BJets && GoodJet_tagged])") \
                        .Histo2D(
                            ROOT.RDF.TH2DModel('btmjetetapt',';abs(eta) vs. pT (b tagged b jet); Jets', *bins), 
                            'GoodMatchedBJets_abseta', 'GoodMatchedBJets_pt', "weight"
                            ),
                    rdf.Define("MatchedCJets_abseta", "abs(GoodJet_eta[CJets])") \
                        .Define("MatchedCJets_pt", "abs(GoodJet_pt[CJets])") \
                        .Histo2D(
                            ROOT.RDF.TH2DModel('cmjetetapt',';abs(eta) vs. pT (c jet); Jets', *bins), 
                            'MatchedCJets_abseta', 'MatchedCJets_pt', "weight"
                            ),
                    rdf.Define("GoodMatchedCJets_abseta", "abs(GoodJet_eta[CJets && GoodJet_tagged])") \
                        .Define("GoodMatchedCJets_pt", "abs(GoodJet_pt[CJets && GoodJet_tagged])") \
                        .Histo2D(
                            ROOT.RDF.TH2DModel('ctmjetetapt',';abs(eta) vs. pT (b tagged c jet); Jets', *bins), 
                            'GoodMatchedCJets_abseta', 'GoodMatchedCJets_pt', "weight"
                            ),
                    rdf.Define("MatchedLightJets_abseta", "abs(GoodJet_eta[LightJets])") \
                        .Define("MatchedLightJets_pt", "abs(GoodJet_pt[LightJets])") \
                        .Histo2D(
                            ROOT.RDF.TH2DModel('lmjetetapt',';abs(eta) vs. pT (light jet); Jets', *bins), 
                            'MatchedLightJets_abseta', 'MatchedLightJets_pt', "weight"
                            ),
                    rdf.Define("GoodMatchedLightJets_abseta", "abs(GoodJet_eta[LightJets && GoodJet_tagged])") \
                        .Define("GoodMatchedLightJets_pt", "abs(GoodJet_pt[LightJets && GoodJet_tagged])") \
                        .Histo2D(
                            ROOT.RDF.TH2DModel('ltmjetetapt',';abs(eta) vs. pT (b tagged light jet); Jets', *bins), 
                            'GoodMatchedLightJets_abseta', 'GoodMatchedLightJets_pt', "weight"
                            ),
                ])

        # histograms with systematic variations (the one used for the measurement)
        rdf = rdf.Define("weight_lne", "weight/GoodTaggedJets_energy")
        bins = [20, 3., 7.]
        title = ';log(E);  1/E dN_{b jets}/dlog(E)'
        name = 'bjetenls'
        if jec_var is None:
            histsVaried.extend([
                ROOT.RDF.Experimental.VariationsFor(h) for h in [
                    rdf.Histo1D((name, title, *bins), "GoodTaggedJets_lne", "weight_lne"),
                ]
            ])
        else:
            name += f"_{jec_var}_{jec_var_sign}"
            histos.extend([
                rdf.Histo1D((name, title, *bins), "GoodTaggedJets_lne", "weight_lne"),
            ])


    # write out histograms to file, initiate the event processing
    print(f"Save histograms in {outFileURL}")
    fOut=ROOT.TFile.Open(outFileURL,'RECREATE')

    for histo in histos: 
        if not is_data:
            histo.Scale(1./weightsum)
        print(f"Save histogram {histo.GetName()}")
        histo.Write()

    for hmap in histsVaried:
        for k in hmap.GetKeys():
            histo = hmap[k]
            if not is_data:
                histo.Scale(1./weightsum)
            parts = str(k).split(":")     
            if len(parts) > 1:
                # get rid of suffix that is automatically appended in name
                new_name = histo.GetName().replace(f'_{parts[0]}', '')                   
                histo.SetName(new_name)

            print(f"Save histogram {histo.GetName()}")
            histo.Write()

    fOut.Close()

def make_jsonhelper(filename="/eos/user/c/cmsdqm/www/CAF/certification/Collisions22/Cert_Collisions2022_355100_362760_Golden.json"):
    """
    Helper to select certified data quality (good lumisections)
    """
    with open(filename) as jsonfile:
        jsondata = json.load(jsonfile)
    
    runs = []
    firstlumis = []
    lastlumis = []
    
    for run,lumipairs in jsondata.items():
        for lumipair in lumipairs:
            runs.append(int(run))
            firstlumis.append(int(lumipair[0]))
            lastlumis.append(int(lumipair[1]))
    
    jsonhelper = ROOT.JsonHelper(runs, firstlumis, lastlumis)
    
    return jsonhelper

def declareCorrectors(era="2022", systematic_variations=True):
    """
        declare corrections using correctionlib, 
        documentation: https://cms-nanoaod.github.io/correctionlib/
        central CMS POG corrections: https://gitlab.cern.ch/cms-nanoAOD/jsonpog-integration/-/tree/master
    """    
    # dictionary to contain custom helpers
    helpers = {}

    correctionlib.register_pyroot_binding()

    base_path = "/cvmfs/cms.cern.ch/rsync/cms-nanoAOD/jsonpog-integration/POG/"

    # pileup
    print(f"Declare pileup correction helper")
    corr_file_pileup = f"{base_path}/LUM/2022_Summer{era[2:]}/puWeights.json.gz"
    corr_name_pileup = "Collisions2022_355100_357900_eraBCD_GoldenJson" if era == "2022" else "Collisions2022_359022_362760_eraEFG_GoldenJson"
    ROOT.gInterpreter.Declare(f'auto corr_pileup = correction::CorrectionSet::from_file("{corr_file_pileup}")->at("{corr_name_pileup}");')

    # muons
    print(f"Declare muon efficiency correction helper")
    corr_file_muon = f"{base_path}/MUO/2022_Summer{era[2:]}/muon_Z.json.gz"
    ROOT.gInterpreter.Declare(f'auto cset_muon = correction::CorrectionSet::from_file("{corr_file_muon}");')
    ROOT.gInterpreter.Declare(f'auto corr_muonID = cset_muon->at("NUM_TightID_DEN_TrackerMuons");')

    # electrons
    print(f"Declare electron efficiency correction helper")
    corr_file_elec = f"{base_path}/EGM/2022_Summer{era[2:]}/electron.json.gz"
    ROOT.gInterpreter.Declare(f'auto corr_eleID = correction::CorrectionSet::from_file("{corr_file_elec}")->at("Electron-ID-SF");')

    # trigger, custom SF measured for TOP-23-008 
    print(f"To be done ... Declare trigger efficiency correction helper")

    # include local helper functions
    ROOT.gInterpreter.AddIncludePath(f"{pathlib.Path(__file__).parent}/include/")
    ROOT.gInterpreter.Declare('#include "helpers.h"')

    # helper to select lumisections with certified data quality
    print(f"Declare good lumisection selection helper")
    helpers["json"] = make_jsonhelper()

    # jet veto maps (see https://cms-jerc.web.cern.ch/Recommendations/#jet-veto-maps)
    print(f"Declare jet veto maps helper")
    corr_file_jetveto = f"{base_path}/JME/2022_Summer{era[2:]}/jetvetomaps.json.gz"
    corr_name_jetveto = "Summer22_23Sep2023_RunCD_V1" if era == "2022" else "Summer22EE_23Sep2023_RunEFG_V1"
    jetvetohelper = ROOT.JetVetoHelper(corr_file_jetveto, corr_name_jetveto)
    helpers["jetveto"] = jetvetohelper

    # jet energy corrections
    print(f"Declare jet energy and pT resolution correction helpers")
    corr_file_jerc = f"{base_path}/JME/2022_Summer{era[2:]}/jet_jerc.json.gz"
    corr_name_jerc = f"Summer{era[2:]}_22Sep2023_V2_MC_L1L2L3Res_AK4PFPuppi"
    helpers["jerc_mc"] = ROOT.JERCHelper(corr_file_jerc, corr_name_jerc)
    if era == "2022":
        helpers["jerc_data"] = ROOT.JERCHelper(corr_file_jerc, "Summer22_22Sep2023_RunCD_V2_DATA_L1L2L3Res_AK4PFPuppi")
    else:
        helpers["jerc_dataE"] = ROOT.JERCHelper(corr_file_jerc, "Summer22EE_22Sep2023_RunE_V2_DATA_L1L2L3Res_AK4PFPuppi")
        helpers["jerc_dataF"] = ROOT.JERCHelper(corr_file_jerc, "Summer22EE_22Sep2023_RunF_V2_DATA_L1L2L3Res_AK4PFPuppi")
        helpers["jerc_dataG"] = ROOT.JERCHelper(corr_file_jerc, "Summer22EE_22Sep2023_RunG_V2_DATA_L1L2L3Res_AK4PFPuppi")

    # Jet pT resolution corrections
    # see https://gitlab.cern.ch/cms-nanoAOD/jsonpog-integration/-/merge_requests/75
    corr_name_jes_sf = f"Summer{era[2:]}_22Sep2023_JRV1_MC_ScaleFactor_AK4PFPuppi"
    corr_name_jes_var = f"Summer{era[2:]}_22Sep2023_JRV1_MC_PtResolution_AK4PFPuppi"
    jer_info = [corr_file_jerc, corr_name_jes_var, corr_name_jes_sf, f"{base_path}/JME/jer_smear.json.gz", "JERSmear",]
    helpers["jer_mc"] = ROOT.JERVarHelper(*jer_info, "nom")

    if systematic_variations:
        # variations of jet energy corrections
        for var in res.jec_variations:
            corr_name_jerc_var = corr_name_jerc.replace("L1L2L3Res", var)
            helpers[f"jerc_mc_{var}"] = ROOT.JESVarHelper(corr_file_jerc, corr_name_jerc_var)

        # variations of jet pT resolution
        helpers["jer_mc_ptResolution_up"] = ROOT.JERVarHelper(*jer_info, "up")
        helpers["jer_mc_ptResolution_down"] = ROOT.JERVarHelper(*jer_info, "down")

    # b-tagging, (following method 1a https://twiki.cern.ch/twiki/bin/viewauth/CMS/BTagSFMethods#1a_Event_reweighting_using_scale)
    corr_file_btag_eff = f"data/eff_btag_TTto2L2Nu_20{era[2:]}.root" # Custom produced file with MC efficiencies
    if os.path.isfile(corr_file_btag_eff):
        print("Declare b tagging event weight based scale factor helpers")
        corr_file_btag = f"{base_path}/BTV/2022_Summer{era[2:]}/btagging.json.gz"
        helpers["btag_b"] = ROOT.BTagHelper(corr_file_btag, "robustParticleTransformer_comb", corr_file_btag_eff, "eff_bjetetapt", "central","M", 5)
        helpers["btag_c"] = ROOT.BTagHelper(corr_file_btag, "robustParticleTransformer_comb", corr_file_btag_eff, "eff_cjetetapt", "central","M", 4)
        helpers["btag_light"] = ROOT.BTagHelper(corr_file_btag, "robustParticleTransformer_light", corr_file_btag_eff, "eff_ljetetapt", "central","M", 0)

        if systematic_variations:
            for var in ["up_uncorrelated", "down_uncorrelated", "up_correlated", "down_correlated"]:
                helpers[f"btag_b_{var}"] = ROOT.BTagHelper(corr_file_btag, "robustParticleTransformer_comb", corr_file_btag_eff, "eff_bjetetapt", var,"M", 5)
                helpers[f"btag_c_{var}"] = ROOT.BTagHelper(corr_file_btag, "robustParticleTransformer_comb", corr_file_btag_eff, "eff_cjetetapt", var,"M", 4)
            for var in ["up", "down"]:
                helpers[f"btag_light_{var}"] = ROOT.BTagHelper(corr_file_btag, "robustParticleTransformer_light", corr_file_btag_eff, "eff_ljetetapt", var,"M", 0)
    else:
        print(f"File {corr_file_btag_eff} with b tagging efficiencies not found, continue without applying b-tagging scale factors.")

    return helpers



def main():
    """
    steer the script
    """

    # configuration and command line options
    parser = argparse.ArgumentParser()
    parser.add_argument('-e', '--era', help='Era to be processed', default="2022", type=str, choices=["2022", "2022EE"])
    parser.add_argument('-o', '--outDir', help='output directory', default='analysis', type=str)
    parser.add_argument('-n', '--nThreads', help='# threads to run in parallel', default=0, type=int)
    parser.add_argument('-m', '--maxFiles', help='# files to process, -1 for all files', default=-1, type=int)
    parser.add_argument('--selectSamples', nargs="*", default=[], help='Select a list of samples to run over')
    parser.add_argument('--measureBtagEfficiencies', help='# produce histograms to measure the b tagging efficiency', action='store_true')
    parser.add_argument('--systematics', help='Evaluate systematic variations', action='store_true')
    args = parser.parse_args()

    # prepare output
    if len(args.outDir)==0: 
        args.outDir='./'

    os.system('mkdir -p %s' % args.outDir)

    helpers = declareCorrectors(args.era, args.systematics)
    
    ROOT.ROOT.EnableImplicitMT(max(0,args.nThreads)) # enable multithreading

    filepaths = res.filepaths[args.era]
    if args.systematics:
        filepaths.update(res.filepaths_systematics[args.era])

    print(f"Process all samples for era {args.era}")
    for sample, paths in filepaths.items():
        if args.measureBtagEfficiencies and sample != "TTto2L2Nu":
            continue
        if args.selectSamples and sample not in args.selectSamples:
            continue
        print(f"Make file list for sample {sample}")
        filelist = []
        for path in paths:
            print(f"Add samples from path {path}")
            filelist.extend(buildFileListXrd(path))

        if len(filelist) <= 0:
            print(f"No files found for sample {sample}, continue with next one")
            continue

        filelist = filelist[:args.maxFiles] if args.maxFiles != -1 and args.maxFiles < len(filelist) else filelist

        outFileURL = f'{args.outDir}/{sample}_{args.era}.root'
        runBJetEnergyPeak(
            filelist, 
            args.era, 
            sample, 
            measure_btag_eff=args.measureBtagEfficiencies, 
            outFileURL=outFileURL, 
            helpers=helpers, 
            do_systematic_variations=args.systematics and sample not in res.filepaths_systematics[args.era]
        )

    ROOT.ROOT.DisableImplicitMT() # disable multithreading


if __name__ == "__main__":
    """
    for execution from another script
    """
    sys.exit(main())
